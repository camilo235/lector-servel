package leerut;

import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.IOException;
import modelo.*;
import java.util.ArrayList;
import java.util.Date;

public class LeeRut {
    private static final String NOM_RUTA = "C:\\Users\\Israel_T\\Documents\\ficheros\\";
    public static void main(String[] args) throws IOException{
        System.out.println("Programa de tabulacion ruts...");
        Excel unExcel = new Excel();
        Textos unTexto = new Textos();
        Manejador manejador = new Manejador(unExcel, unTexto, NOM_RUTA);
        manejador.poblaDatos();
    }
    
}
