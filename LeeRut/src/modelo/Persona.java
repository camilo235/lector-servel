package modelo;

import java.util.Date;

public class Persona {
    private String rut, nombres, apaterno, amaterno;
    private Date fechaNac;

    public Persona(String rut, String nombres, String apaterno, String amaterno, Date fechaNac) {
        this.rut = rut;
        this.nombres = nombres;
        this.apaterno = apaterno;
        this.amaterno = amaterno;
        this.fechaNac = fechaNac;
    }
    public Persona() {
    }

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApaterno() {
        return apaterno;
    }

    public void setApaterno(String apaterno) {
        this.apaterno = apaterno;
    }

    public String getAmaterno() {
        return amaterno;
    }

    public void setAmaterno(String amaterno) {
        this.amaterno = amaterno;
    }

    public Date getFechaNac() {
        return fechaNac;
    }

    public void setFechaNac(Date fechaNac) {
        this.fechaNac = fechaNac;
    }
    
    @Override
    public String toString()
    {
        StringBuilder sb =new StringBuilder();
        sb.append("Nombres: ").
                append(nombres).append(" ")
                .append(" Apellidos: ").append(apaterno).append(amaterno).append(" RUT: ")
                .append(rut)
        .append(" fecha nacimiento: ").append(fechaNac);
        
        return sb.toString();
    }
}
