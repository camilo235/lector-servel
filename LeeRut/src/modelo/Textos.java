package modelo;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.io.FileReader;
import java.util.logging.Level;
import java.util.logging.Logger;

//Procesamiento de archivos de textoss
public class Textos {

    private Persona persona;

    public Textos() {

    }

    public void leeArchivo(String ruta, Excel excel) {

        String amaterno = "", apaterno = "", rut = "", nombres = "", nomCompleto = "";
        Date fechanac = new Date();
        if (ruta != null && excel != null) {

            File dir = new File(ruta);
                //Columna inicial de excel inicializado como 1
                excel.setRowNum(1);
            for (File archivo : dir.listFiles()) {
                try (BufferedReader br = new BufferedReader(new FileReader(archivo))) {
                    String linea;
                    while ((linea = br.readLine()) != null) {
                        nomCompleto = linea.substring(19, 55);
                        rut = linea.substring(10, 19);
                        DateFormat formato = new SimpleDateFormat("yyyyMMdd");
                        fechanac = (Date) formato.parse(linea.substring(55, 63));
                        String[] noms = nomCompleto.split(" ");
                        apaterno = noms[0];
                        amaterno = noms[1];
                        nombres = "";
                        if (noms.length > 3) {
                            for (int i = 2; i < noms.length; i++) {
                                nombres += noms[i];
                            }
                        } else {
                            nombres = noms[2];
                        }

                        if (amaterno != null && apaterno != null && fechanac != null && nombres != null && rut != null) {
                            persona = new Persona();
                            persona.setAmaterno(amaterno);
                            persona.setApaterno(apaterno);
                            persona.setFechaNac(fechanac);
                            persona.setNombres(nombres);
                            persona.setRut(rut);
                            System.out.println(rut +" "+apaterno+" "+amaterno+" leido");
                        }

                        excel.InsertData(persona);
                        System.out.println("RUT " + rut + " insertado");
                        excel.setRowNum(excel.getRowNum() + 1);
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (ParseException ex) {
                    Logger.getLogger(Textos.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
}
