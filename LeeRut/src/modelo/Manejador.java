package modelo;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.usermodel.HSSFRow;

public class Manejador {
   private Excel excel;
   private Textos textos;
   private String ruta;
   
   public Manejador(Excel excel, Textos textos, String ruta){
       this.excel = excel;
       this.textos = textos;
       this.ruta = ruta;
   }
   
   public void poblaDatos(){
       excel.CreateHeaderCells();
       textos.leeArchivo(ruta, excel);
   }
   
}

