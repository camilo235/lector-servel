package modelo;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

public class Excel {
    private final int RUT = 0, NOMBRES = 1, APATERNO = 2, AMATERNO = 3, FECHANAC = 4;
    private int rowNum;
    
    public Excel() {
    }
    
    /*
    private List<Persona> personas = new ArrayList();
    
    {
        Calendar fechaNac = Calendar.getInstance();
        fechaNac.set(1961,10,17);
        personas.add(new Persona("8497644K","VERONICA SILVIA","ORTIZ","ROJAS",fechaNac.getTime()));
    
    
        fechaNac.set(1993,10,05);
        personas.add(new Persona("18661982K","CAMILO ALBERTO","JUSID","ORTIZ",fechaNac.getTime()));    
   
        fechaNac.set(1961,10,17);
        personas.add(new Persona("8497644K","VERONICA SILVIA","ORTIZ","ROJAS",fechaNac.getTime()));
    
   
        fechaNac.set(1961,10,17);
        personas.add(new Persona("8497644K","VERONICA SILVIA","ORTIZ","ROJAS",fechaNac.getTime()));
        
    }
     */
    
    private final String[] columnas =  {"RUT","NOMBRES","APATERNO","AMATERNO","FECHANAC"};
    Workbook workbook = new HSSFWorkbook();
    CreationHelper createHelper = workbook.getCreationHelper();
    
    Sheet hoja = workbook.createSheet("Personas prueba");
    
    Row headerRow = hoja.createRow(0);

    public int getRowNum() {
        return rowNum;
    }

    public void setRowNum(int rowNum) {
        this.rowNum = rowNum;
    }
    
    public void CreateHeaderCells(){
        for(int i =0; i<columnas.length; i++){
           Cell cell = headerRow.createCell(i);
           cell.setCellValue(columnas[i]);
           
        }
        System.out.println("Cabeceras creadas");
    }
    public void InsertData(Persona persona) throws FileNotFoundException, IOException{
            Row row = hoja.createRow(rowNum);
            row.createCell(RUT).setCellValue(persona.getRut());
            row.createCell(NOMBRES).setCellValue(persona.getNombres());
            row.createCell(APATERNO).setCellValue(persona.getApaterno());
            row.createCell(AMATERNO).setCellValue(persona.getAmaterno());
            
            Cell celdaFechaNac = row.createCell(FECHANAC);
            celdaFechaNac.setCellValue(persona.getFechaNac());
            //falta agregar estilo fecha para formato
            
        //Cambia tamaño de la celda segun contenido
        for(int i = 0; i<columnas.length; i++){
            hoja.autoSizeColumn(i);
        }
        //Escribe la salida en un archivo
        FileOutputStream salida = new FileOutputStream("C:\\Users\\Israel_T\\Documents\\pruebas\\pruebaExcel.xls");
        workbook.write(salida);
        salida.close();
        workbook.close();
    }
    
}
